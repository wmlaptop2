#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <X11/Xlib.h>
#include <dbus/dbus.h>

int get_light_sensor_value(void)
{

	int fd;
	size_t i,n=0;
	char buf[10];
	char a_light[10];
	const char *light_sensor="/sys/devices/platform/applesmc.768/light";
	ssize_t cnt;

	/* read light sensor value */
	fd = open(light_sensor, O_RDONLY);
	if (fd < 0) {
		perror(light_sensor);
                fprintf(stderr, "Can't open %s\n", light_sensor);
		exit(1);
	}
	cnt = read(fd, buf, sizeof(buf)-1);
	buf[cnt] = '\0';
	close(fd);

	/* convert light sensor string value to integer */
	for (i = 0; buf[i] != '\0'; i++) {
		if (buf[i] == ',')
			break;
		if (buf[i] != '(') {
			a_light[n] = buf[i];
			n++;
		}
	}
	a_light[n] = '\0';

	return atoi(a_light);
}

int set_keyboard_brightness_value(int brightness)
{

	DBusConnection *connection;
	DBusError error;
	DBusMessage *message;
	DBusMessageIter iter;
	DBusBusType type;

	const char *name = NULL;
	char arg[] = "org.freedesktop.UPower.KbdBacklight.SetBrightness";
	name = arg;
	const char *dest = "org.freedesktop.UPower";
	const char *path = "/org/freedesktop/UPower/KbdBacklight";

	type = DBUS_BUS_SYSTEM;

	dbus_error_init(&error);
	connection = dbus_bus_get(type, &error);

	if (connection == NULL) {
		fprintf (stderr, "Failed to open connection to message bus: %s\n", error.message);
		dbus_error_free(&error);
		return (-1);
	}

	char *last_dot;
	last_dot = strrchr(name, '.');
	*last_dot = '\0';

	message = dbus_message_new_method_call(NULL, path, name, last_dot + 1);
	dbus_message_set_auto_start(message, TRUE);

	if (message == NULL) {
		fprintf (stderr, "Couldn't allocate D-Bus message\n");
		return (-1);
	}

	if (dest && !dbus_message_set_destination (message, dest)) {
		fprintf (stderr, "Not enough memory\n");
		return (-1);
	}

	dbus_message_iter_init_append(message, &iter);

	dbus_int32_t int32;
	int32 = brightness;
	dbus_message_iter_append_basic(&iter, DBUS_TYPE_INT32, &int32);

	dbus_connection_send(connection, message, NULL);
	dbus_connection_flush(connection);
	dbus_message_unref(message);
	dbus_connection_unref(connection);
	return 0;
}
