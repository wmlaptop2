/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef  __BATTERY_H__
#define  __BATTERY_H__

#include "main.h"


/*******************
 * ACPI FEATURES   *
 *********************************************************************************/
#define    ACPI_ACCESS_TEST     "/sys/module/acpi/parameters/acpica_version" //"/proc/acpi/info"

/* checks if it's possible to use ACPI support */
bool ACPI_canSupport( );



/* common function for ACPI and APM support that redraw the dockapp if battery
 * state is changed. */
void setNewBatteryState( );

void read_power();

void battery_capacity();

void print_battery();

#endif
