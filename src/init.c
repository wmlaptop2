/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "init.h"


void init_display(void)
{
	XClassHint       classHint;
	XTextProperty    name;
	XGCValues        gcv;
	unsigned long    gcm;
	int              dummy=0;
	char             *progname = PROGNAME;
	Pixel            back_pix;
	Pixel            fore_pix;

	display = XOpenDisplay(XDisplay);

	if(display == NULL) {
		fprintf(stderr, "Can't open display\n" );
		free_and_exit( ERROR );
	}

	screen  = DefaultScreen(display);
	Root    = RootWindow(display, screen);
	d_depth = DefaultDepth(display, screen);
	x_fd    = XConnectionNumber(display);


	init_image ( );

	/* Create a window to hold the stuff */
	mysizehints.flags = USSize | USPosition;
	mysizehints.x = 0;
	mysizehints.y = 0;

	back_pix = WhitePixel( display, screen);
	fore_pix = BlackPixel( display, screen);

	XWMGeometry(display, screen, NULL, NULL, 1, &mysizehints, \
	            &mysizehints.x, &mysizehints.y,&mysizehints.width,&mysizehints.height, &dummy);

	mysizehints.width = 64;
	mysizehints.height = 64;

	win = XCreateSimpleWindow(display, Root, mysizehints.x, mysizehints.y, \
	                          mysizehints.width, mysizehints.height, 1, fore_pix, back_pix);

	iconwin = XCreateSimpleWindow(display, win, mysizehints.x, mysizehints.y, \
	                              mysizehints.width, mysizehints.height, 1, fore_pix, back_pix);

	/* Activate hints */
	XSetWMNormalHints(display, win, &mysizehints);
	classHint.res_name = progname;
	classHint.res_class = progname;
	XSetClassHint(display, win, &classHint);

	XSelectInput(display, win, ButtonPressMask | ButtonReleaseMask | ExposureMask | StructureNotifyMask );
	XSelectInput(display, iconwin, ButtonPressMask | ButtonReleaseMask | ExposureMask | StructureNotifyMask );

	if (XStringListToTextProperty(&progname, 1, &name) == 0)
		PRINTQ(stderr, "%s: can't allocate window name\n", PROGNAME);

	XSetWMName(display, win, &name);
	/* Create GC for drawing */
	gcm = GCForeground | GCBackground | GCGraphicsExposures;
	gcv.foreground = fore_pix;
	gcv.background = back_pix;
	gcv.graphics_exposures = 0;
	NormalGC = XCreateGC(display, Root, gcm, &gcv);

	/* ONLYSHAPE ON */
	pixmask = XCreateBitmapFromData(display, win, wmlaptop_mask_bits, wmlaptop_mask_width, wmlaptop_mask_height);

	XShapeCombineMask(display, win, ShapeBounding, 0, 0, pixmask, ShapeSet);
	XShapeCombineMask(display, iconwin, ShapeBounding, 0, 0, pixmask, ShapeSet);

	/* ONLYSHAPE OFF */
	mywmhints.initial_state = WithdrawnState;
	mywmhints.icon_window = iconwin;
	mywmhints.icon_x = mysizehints.x;
	mywmhints.icon_y = mysizehints.y;
	mywmhints.window_group = win;
	mywmhints.flags = StateHint | IconWindowHint | IconPositionHint | WindowGroupHint;

	XSetWMHints(display, win, &mywmhints);
	XMapWindow(display, win);
}


void init_image()
{
	char *xbm = wmlaptop_mask_bits;
	char ** xpm;
	int sx = wmlaptop_mask_width;
	int sy = wmlaptop_mask_height;

	XWindowAttributes      attributes;
	int                    err;
	int i,j,k;
	int width, height, numcol, depth;
	int zero=0;
	int bcount;
	int curpixel;
	unsigned char bwrite;


	usePixmap();
	xpm = XPM_NAME;

	sscanf(*xpm, "%d %d %d %d", &width, &height, &numcol, &depth);


    for (k=0; k!=depth; k++) {
	    zero <<=8;
	    zero |= xpm[1][k];
    }

	for (i=numcol+1; i < numcol+sy+1; i++) {
		bcount = 0;
		bwrite = 0;
		for (j=0; j<sx*depth; j+=depth) {
			bwrite >>= 1;
			curpixel=0;
			for (k=0; k!=depth; k++) {
				curpixel <<=8;
				curpixel |= xpm[i][j+k];
			}

			if (curpixel != zero)
				bwrite += 128;

			bcount++;
			if (bcount == 8) {
				*xbm = bwrite;
				xbm++;
				bcount = 0;
				bwrite = 0;
			}
		}
	}

	/* For the colormap */
	XGetWindowAttributes(display, Root, &attributes);

	wmgen.attributes.valuemask |= (XpmReturnPixels | XpmReturnExtensions);

	err = XpmCreatePixmapFromData(display, Root, XPM_NAME, &(wmgen.pixmap), &(wmgen.mask), &(wmgen.attributes));
	if (err != XpmSuccess) {
		fprintf(stderr, "Not enough free colorcells.\n");
		free_and_exit( ERROR );
	}
}
