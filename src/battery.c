/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "battery.h"
#include <dirent.h>

void read_sysfs_value(char *dir, char *file, double *value) {
	char filename[256];
	FILE *fp;

	snprintf(filename, 256, "/sys/class/power_supply/%s/%s", dir, file);
	if ((fp = fopen(filename, "r"))) {
		fscanf(fp, "%lf", value);
                (*value) /= 1000000.;
		fclose(fp);
        }
}

bool ACPI_canSupport( )
{
	if(access(ACPI_ACCESS_TEST, R_OK ) == SUCCESS)
		return true;
	return false;
}

/* Total power usage (sum over all batteries).
 * When there is no battery present, we set powerrate to zero */
void read_power()
{
	DIR *dir;
	struct dirent *dirent;
        double powerrate_bat;
	extern double powerrate;
        powerrate = 0;

	dir = opendir("/sys/class/power_supply");
	if (!dir)
		return;

	while ((dirent = readdir(dir))) {
		if (strlen(dirent->d_name) < 3)
			continue;
		double current_bat = 0, voltage_bat = 0;
		read_sysfs_value(dirent->d_name, "current_now", &current_bat);
		if (current_bat <= 0.001)
			current_bat = 0;
		read_sysfs_value(dirent->d_name, "voltage_now", &voltage_bat);
		powerrate_bat = current_bat*voltage_bat;
		read_sysfs_value(dirent->d_name, "power_now", &powerrate_bat);

                powerrate += powerrate_bat;
	}
	closedir(dir);

#ifdef DEBUG
	fprintf(stdout,"powerrate = %3.1f\n", powerrate);
#endif
}

/* Computes the sum of 'last full capacity' for all batteries */
void battery_capacity()
{
	DIR *dir;
	struct dirent *dirent;
        double cap_bat;
	extern double cap_total;
        cap_total = 0;

	dir = opendir("/sys/class/power_supply");
	if (!dir)
		return;

	while ((dirent = readdir(dir))) {
		double charge_bat = 0, voltagemin_bat = 0, voltagenow_bat = 0;
		if (strlen(dirent->d_name) < 3)
			continue;
		read_sysfs_value(dirent->d_name, "charge_full", &charge_bat);
                read_sysfs_value(dirent->d_name, "voltage_min_design", &voltagemin_bat);
                read_sysfs_value(dirent->d_name, "voltage_now", &voltagenow_bat);
			cap_bat = charge_bat*(voltagemin_bat>voltagenow_bat?voltagemin_bat:voltagenow_bat);
		read_sysfs_value(dirent->d_name, "energy_full", &cap_bat);
                cap_total += cap_bat;
	}
	closedir(dir);

#ifdef DEBUG
	fprintf(stdout,"Total battery capacity = %3.1f Wh \n", cap_total);
#endif
}

void print_battery(void)
{
	DIR *dir;
	struct dirent *dirent;
	FILE *file;
	double cap_left = 0.0;
	extern double cap_total;
	char filename[256];

	/* battery wasn't connected when wmlaptop started */
	if (cap_total == 0)
		battery_capacity();

	dir = opendir("/sys/class/power_supply");
	if (!dir)
		return;

	read_power();
	powerState.isCharging = false;

	while ((dirent = readdir(dir))) {
	  double watthours_left = 0.0, charge_bat = 0, voltage_bat = 0;;
		char line[1024];

		if (strlen(dirent->d_name) < 3)
			continue;

		sprintf(filename, "/sys/class/power_supply/%s/status", dirent->d_name);
		file = fopen(filename, "r");
		if (!file)
			continue;
		memset(line, 0, 1024);
		fgets(line, 1024, file);
                if (!strstr(line, "Discharging")) {
			powerState.isCharging = true;
		}
                fclose(file);

		read_sysfs_value(dirent->d_name, "charge_now", &charge_bat);
                read_sysfs_value(dirent->d_name, "voltage_now", &voltage_bat);
			watthours_left = charge_bat*voltage_bat;
                read_sysfs_value(dirent->d_name, "energy_now", &watthours_left);
                if (watthours_left > cap_total)
			/* Some batteries are buggy, and report
			 * design capacity when they are full */
			watthours_left = cap_total;

                cap_left += watthours_left;

		if (!powerState.isCharging) {
                        if (powerrate <= 0.)
				powerState.remainingTime = 0.;
                        else
				powerState.remainingTime =  60*cap_left/powerrate;
#ifdef DEBUG
			fprintf(stdout,"taxa = %3.1fW horas=%3.1f \n", powerrate, cap_left/powerrate);
#endif
		}
		else {
#ifdef DEBUG
			fprintf(stdout,"watt-hours left = %3.1f\n", watthours_left);
#endif
			/* time to charge battery at 100% */
                        if (powerrate <= 0.)
				powerState.remainingTime = 0.;
                        else
				powerState.remainingTime = 60*(cap_total - watthours_left)/powerrate;
                }

		if (watthours_left <= cap_total)
			powerState.percentage = 100*(cap_left/cap_total);
                else
			powerState.percentage = 100;

#ifdef DEBUG
		fprintf(stdout,"Battery capacity left = %3.1f Wh \n", cap_left);
		fprintf(stdout,"Total capacity = %3.1f Wh \n", cap_total);
		fprintf(stdout,"Battery percentage = %d \n", powerState.percentage);
		fprintf(stdout,"Total power usage = %3.1f W \n", powerrate);
#endif

	}
	closedir(dir);
}
