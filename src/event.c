/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "event.h"
#include "apple.h"
#include "main.h"

int brightness = 0; /* initial state assumption keyboard at 0% brightness */
int max_brightness = 100; /* adjusted later with get_screen_brightness() */
int min_brightness = 20;  /* idem */
int at_minimum = 0;

void event_handler()
{
	u_int32 update_seconds, update_seconds2, update_seconds3;
	int j, initial_temp_update = temp_update;
	secondsCounter = time(NULL);
	update_seconds = time(NULL);
	update_seconds2 = update_seconds;
	update_seconds3 = update_seconds;

	while (1) {
		secondsCounter = time(NULL);

		if (XPending(display)) {
			XNextEvent(display, &e);

			switch (e.type) {
			case Expose:
				draw_all();
				break;

			case ButtonPress:
				j = CheckMouseRegion(e.xbutton.x, e.xbutton.y);
				/* if a cliccable region was clicked, the dockapp is redrawn */
				if (j != -1)
					draw_all();
				break;
			}
		}

		if (cpuLoad > 45)
			temp_update = 2;
		else
			temp_update = initial_temp_update;

		if (update_seconds <= secondsCounter - battery_update) {
			update_seconds = secondsCounter;
			print_battery();

			if (auto_shutdown != 0)
				checkAutoShutdown();
			if (auto_alarm != 0)
				checkAutoAlarm();
		}

		if (update_seconds2 <= secondsCounter - temp_update) {
			update_seconds2 = secondsCounter;
			cpuReadTemp();
		}

		if (update_seconds3 <= secondsCounter - power_update) {
			update_seconds3 = secondsCounter;
			read_power();
		}

		manage_brightness();

		/* check and redraw cpu load */
		cpuLoad = getCpuLoad();
		cpuReadFreq();
		draw_all();
		usleep(1000 * cpu_update);
	}
}

void AddMouseRegion(u_int8 index, u_int8 left, u_int8 top, u_int8 right,
		    u_int8 bottom)
{
	if (index < MAX_MOUSE_REGION) {
		mouse_region[index].top = top;
		mouse_region[index].left = left;
		mouse_region[index].bottom = bottom;
		mouse_region[index].right = right;
	}
}

u_int8 CheckMouseRegion(u_int8 x, u_int8 y)
{
	register int i;
	register bool found;

	found = 0;

	for (i = 0; i < MAX_MOUSE_REGION && !found; i++) {
		if (x <= mouse_region[i].right &&
		    x >= mouse_region[i].left &&
		    y <= mouse_region[i].bottom && y >= mouse_region[i].top)
			found = 1;
	}
	if (!found)
		return -1;
	return (i - 1);
}

bool flashingLowBatteryCycle;

void stopFlashingLowBattery(int useless)
{
	flashingLowBatteryCycle = false;
	wait(NULL);
	return;
}

void startFlashingLowBattery(pid_t childPid)
{
	bool alternate = false;
	int i;

	while (flashingLowBatteryCycle) {
		/* we make the child die if the user click on the dockapp */
		if (XPending(display)) {
			XNextEvent(display, &e);
			switch (e.type) {
			case ButtonPress:
				/* kill the child */
				kill(childPid, SIGKILL);
			}
		}

		if (alternate)
			draw_area(64, 0, 59, 59, 2, 2);
		else
			draw_area(64, 59, 59, 59, 2, 2);

		alternate = !alternate;

		while (XCheckTypedWindowEvent(display, iconwin, Expose, &e)) ;
		XCopyArea(display, wmgen.pixmap, iconwin, NormalGC, 0, 0,
			  wmgen.attributes.width, wmgen.attributes.height, 0,
			  0);
		while (XCheckTypedWindowEvent(display, win, Expose, &e)) ;
		XCopyArea(display, wmgen.pixmap, win, NormalGC, 0, 0,
			  wmgen.attributes.width, wmgen.attributes.height, 0,
			  0);

		usleep(80000);
	}

	/* now i have to fill the dockapp background with black.. ugly job */
	for (i = 2; i < 60; i += 20)
		draw_area(64, 119, 59, 20, 2, i);

	while (XCheckTypedWindowEvent(display, iconwin, Expose, &e)) ;
	XCopyArea(display, wmgen.pixmap, iconwin, NormalGC, 0, 0,
		  wmgen.attributes.width, wmgen.attributes.height, 0, 0);
	while (XCheckTypedWindowEvent(display, win, Expose, &e)) ;
	XCopyArea(display, wmgen.pixmap, win, NormalGC, 0, 0,
		  wmgen.attributes.width, wmgen.attributes.height, 0, 0);

}

void manage_brightness(void)
{
	char buf[64];

	/* Update the max (min) backlight values while at max (min) brightness. So
	 * the user decides which values to use by manually changing them. */
	if (at_minimum)
		min_brightness = get_screen_brightness();
	else
		max_brightness = get_screen_brightness();

	if (!powerState.isCharging) {
		if (get_session_idle_time(display) >= session_idle) {
			if (manage_backlight == 1&& !at_minimum) {
				sprintf(buf, "xbacklight -set %d", min_brightness);
				system(buf);
				at_minimum = 1;
			}

			if (apple == 1 && brightness == 255) {
				set_keyboard_brightness_value(0);
				brightness = 0;
			}
		} else {
			/* There's activity. Turn on lights even though on battery */
			if (manage_backlight == 1&& at_minimum) {
				sprintf(buf, "xbacklight -set %d", max_brightness);
				system(buf);
				at_minimum = 0;
			}
			if (apple == 1 && brightness == 0 && get_light_sensor_value() <= 3) {
				set_keyboard_brightness_value(255);
				brightness = 255;
			}
		}
	} else {
		if (manage_backlight == 1 && at_minimum){
			sprintf(buf, "xbacklight -set %d", max_brightness);
			system(buf);
			at_minimum = 0;
		}
		if (apple == 1 && brightness == 0 && get_light_sensor_value() <= 3) {
			set_keyboard_brightness_value(255);
			brightness = 255;
		}
		if (apple == 1 && brightness == 255 && get_light_sensor_value() > 3) {
			set_keyboard_brightness_value(0);
			brightness = 0;
		}
	}
}
