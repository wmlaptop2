/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <X11/extensions/scrnsaver.h>
#include "main.h"

static void print_settings(void);

/* cpu's stuff */
struct cpuFreq cpuState;
       u_int8 cpuLoad;

/* battery's stuff */
struct power_str powerState;

/* time counter */
       time_t secondsCounter;

/* command line's args */
       int cpu_update        = 800; // miliseconds
       int power_update      = 5;   // seconds
       int temp_update       = 5;   // seconds
       int battery_update    = 10;  // seconds
       int session_idle      = 20;  // seconds
       char *XDisplay        = NULL;
       char auto_shutdown    = 1;
       short shutdown_delay  = 1;
       char auto_alarm       = 5;   // play alarm with the speaker
       bool quiet            = false;
       bool dontBlink100     = false;
       double powerrate      = 1;
       double cap_total      = 0;
       int apple             = 0;
       int manage_backlight  = 0;

/* X stuff */
struct mouseRegion mouse_region[MAX_MOUSE_REGION];

       Display     *display;
       Window      Root;
       Window      iconwin;
       Window      win;
       int         screen;
       int         x_fd;
       int         d_depth;
       XSizeHints  mysizehints;
       XWMHints    mywmhints;
       GC          NormalGC;
       XEvent      e;
struct XpmIcon     wmgen;
       Pixmap      pixmask;

       char        wmlaptop_mask_bits[64 * 64];
       int         wmlaptop_mask_width = 64;
       int         wmlaptop_mask_height = 64;


int main(int argc, char *argv[])
{
	setArgs(argc,argv);
	init_display();

	if(!ACPI_canSupport()){
		fprintf(stderr, "Cannot support ACPI (the file %s doesn't exist)\n", ACPI_ACCESS_TEST);
		fprintf(stderr, ".. and now ? ... I must die\n");
		free_and_exit(ERROR);
	}

	battery_capacity();

	if (quiet == false)
		print_settings();

	cpuReadTemp();
	print_battery();
	event_handler();
	return SUCCESS;
}

void free_and_exit(int code)
{
	if(display)
		XCloseDisplay(display);
	exit(code);
}


/* prints help and exits */
void usage(char *prog, char *unknowOpt)
{
	fprintf(stderr, "%s dockapp (version %s)\n",
	         PROGNAME, VERSION);
	fprintf(stderr, "It shows battery, cpu and power information\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "[usage]: %s [options]\n", prog );
	fprintf(stderr, "\n");
	fprintf(stderr, "[X-options]:\n");
	fprintf(stderr, "   --display=NAME             set the display to open\n");
	fprintf(stderr, "   --dontblink                if you dont want to see \"CPULOAD\" blinking at 100%%\n");
	fprintf(stderr, "   --backlight                dim lcd brightness upon inactivity when on battery\n");
	fprintf(stderr, "   --session-idle=N           X session inactivity time in seconds after which backlight is dimmed\n");
	fprintf(stderr, "   --apple                    Manage keyboard backlight on Apple laptops\n");
	fprintf(stderr, "[AUTOSCRIPT-options]:\n");
	fprintf(stderr, "   --auto-shutdown=off|N      enable autoshutdown when battery percentage is at\n");
	fprintf(stderr, "                              N%% (and AC adapter is not plugged in), or disable\n");
	fprintf(stderr, "                              it with 'off'. ('/sbin/shutdown' is used)\n");
	fprintf(stderr, "   --shutdown-delay=N         insert a delay in minutes to the shutdown call\n");
	fprintf(stderr, "   --auto-alarm=off|N         enable the speaker to play a alarm when battery\n");
	fprintf(stderr, "                              percentage is at N%%. (or diseable it with 'off')\n");
	fprintf(stderr, "[GENERAL-options]:\n");
	fprintf(stderr, "   --cpu-update=N             how often, in milliseconds, to update CPU display\n");
	fprintf(stderr, "   --battery-update=N         how often, in seconds, to update battery state\n");
        fprintf(stderr, "   --power-update=N           how often, in seconds, to update power state\n");
	fprintf(stderr, "   --temperature-update=N     how often, in seconds, to update temperature state\n");
	fprintf(stderr, "   -q   --quiet               do not print messages and warnings\n");
	fprintf(stderr, "   -v   --version             show the version, and exit\n");
	fprintf(stderr, "   -h   --help                show this message, and exit\n");
	fprintf(stderr, "\n");

	if(unknowOpt) {
		fprintf(stderr, "Unkown Options: %s\n", unknowOpt);
		fprintf(stderr, "\n");
		free_and_exit(ERROR);
	}
	free_and_exit(SUCCESS);
}

void version()
{
	fprintf(stderr, "%s dockapp, version %s\n", PROGNAME, VERSION);
	fprintf(stderr, "\n");
	fprintf(stderr, "Author: Giacomo, alias ]Matic[, Galilei:  matic at libero dot it\n");
	fprintf(stderr, "Author: Lorenzo, alias ]StClaus[, Marcon: stclaus at libero dot it\n");
	fprintf(stderr, "Author: Carlos R. Mafra crmafra at gmail.com\n");
	fprintf(stderr, "\n");
	free_and_exit(SUCCESS);
}

void setArgs(int argc, char **argv)
{
	register int i;
	char *ptr;

	for(i = 1; i < argc; i++) {
		if(!strncmp(argv[i], "--display=", 10)) {
			XDisplay = strchr(argv[i], '=');
			XDisplay++;
			continue;
		}
		if(!strcmp(argv[i], "--dontblink")){
			dontBlink100 = true;
			continue;
		}
		if(!strcmp(argv[i], "--apple")){
			apple = 1;
			continue;
		}
		if(!strcmp(argv[i], "--backlight")){
			manage_backlight = 1;
			continue;
		}
		if(!strncmp(argv[i], "--auto-shutdown=", 16)) {
			if(!strcmp( &argv[i][16], "off"))
				auto_shutdown = 0;
			else
				auto_shutdown = atoi(&argv[i][16]);
			continue;
		}
		if(!strncmp( argv[i], "--shutdown-delay=", 17 ) ) {
			shutdown_delay = atoi( &argv[i][17] );
			continue;
		}
		if(!strncmp( argv[i], "--auto-alarm=", 13 ) ) {
			if(!strcmp( &argv[i][13], "off" ) )
				auto_alarm = 0;
			else
				auto_alarm = atoi( &argv[i][13] );
			continue;
		}
		if(!strncmp(argv[i], "--cpu-update=", 13 ) ) {
			ptr = strchr(argv[i], '=');
			ptr++;
			cpu_update = atoi(ptr);
			continue;
		}
		if(!strncmp(argv[i], "--battery-update=", 17)) {
			ptr = strchr(argv[i], '=');
			ptr++;
			battery_update = atoi(ptr);
			continue;
		}
		if(!strncmp(argv[i], "--power-update=", 11)) {
			ptr = strchr(argv[i], '=' );
			ptr++;
			power_update = atoi(ptr);
			continue;
		}
		if(!strncmp(argv[i], "--temperature-update=", 11)) {
			ptr = strchr(argv[i], '=');
			ptr++;
			temp_update = atoi(ptr);
			continue;
		}
		if(!strncmp(argv[i], "--session-idle=", 11)) {
			ptr = strchr(argv[i], '=');
			ptr++;
			session_idle = atoi(ptr);
			continue;
		}
		if(!strcmp(argv[i], "-q") || !strcmp(argv[i], "--quiet")) {
			quiet = 1;
			continue;
		}
		if(!strcmp(argv[i], "-v") || !strcmp(argv[i], "--version"))
			version( );
		if(!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
			usage(argv[0], NULL );
		usage(argv[0], argv[i]);
	}
}

static void print_settings(void)
{
	fprintf(stdout, "XDisplay = %s\n",  XDisplay == NULL ? "NULL" : XDisplay);
	fprintf(stdout, "CPU update = %d millisecs \n", cpu_update);
	fprintf(stdout, "Temperature update = %d secs \n", temp_update);
	fprintf(stdout, "Power update = %d secs \n", power_update);
	fprintf(stdout, "Battery update = %d secs \n", battery_update);
	fprintf(stdout, "Session Idle = %d secs\n", session_idle);

	if (auto_shutdown != 0)
		fprintf( stdout, "Auto shutdown ON\n");
	else
		fprintf( stdout, "Auto shutdown OFF\n");

	fprintf( stdout, "Shutdown delay = %d secs\n", shutdown_delay);

	if (auto_alarm != 0)
		fprintf(stdout, "Auto alarm ON\n");
	else
		fprintf(stdout, "Auto alarm OFF\n");

	if (manage_backlight == 1)
		fprintf(stdout, "Manage lcd backlight ON\n");
	else
		fprintf(stdout, "Manage lcd backlight OFF\n");

	if (apple == 1)
		fprintf(stdout, "Manage apple keyboard light ON\n");
	else
		fprintf(stdout, "Manage apple keyboard light OFF\n");
}

float get_session_idle_time(Display *display)
{

	XScreenSaverInfo info;
	float seconds;

	XScreenSaverQueryInfo(display, DefaultRootWindow(display), &info);
	seconds = (float)info.idle/1000.0f;
	return(seconds);
}

int get_screen_brightness(void)
{

	int fd, actual_backlight, max_backlight;
	char buf[5];
	struct stat tmp;
	const char *scr_backlight="/sys/class/backlight/acpi_video0/actual_brightness";
	const char *scr_maxbacklight="/sys/class/backlight/acpi_video0/max_brightness";
	ssize_t cnt;

	if (stat(scr_backlight, &tmp) == 0) {

		/* read screen backlight value */
		fd = open(scr_backlight, O_RDONLY);
		if (fd < 0) {
			perror (scr_backlight);
			fprintf (stderr, "Can't open %s\n",scr_backlight);
			exit(1);
		}
		cnt=read(fd, buf, sizeof(buf)-1);
		buf[cnt]='\0';
		close(fd);
		actual_backlight=atoi(buf);

		/* read screen max backlight value */
		fd = open(scr_maxbacklight, O_RDONLY);
		if (fd < 0) {
			perror (scr_backlight);
			fprintf (stderr, "Can't open %s\n",scr_backlight);
			exit(1);
		}
		cnt=read(fd, buf, sizeof(buf)-1);
		buf[cnt]='\0';
		close(fd);
		max_backlight=atoi(buf);

		/* make sure we always retrn a value between 0 and 100% */
		return (100*actual_backlight)/max_backlight;

	} else {
		return 0;

	}
}
