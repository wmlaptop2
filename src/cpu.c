/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "cpu.h"

/* thanks to wmfire */
u_int8 getCpuLoad()
{
	static int lastloadstuff = 0, lastidle = 0, lastnice = 0;
	int loadline = 0, loadstuff = 0, nice = 0, idle = 0;

	FILE *stat = fopen("/proc/stat", "r");
	char temp[128], *p;

	if (!stat) {
		static bool errorFlag = false;
		if (!errorFlag) {
			PRINTQ(stderr,
			       "Error reading the file '/proc/stat' for cpu-load info\n");
			errorFlag = true;
		}
		return 100;
	}

	while (fgets(temp, 128, stat)) {
		if (!strncmp(temp, "cpu", 3) && temp[3] == ' ') {
			p = strtok(temp, " \t\n");
			loadstuff = atol(p = strtok(NULL, " \t\n"));
			nice = atol(p = strtok(NULL, " \t\n"));
			loadstuff += atol(p = strtok(NULL, " \t\n"));
			idle = atol(p = strtok(NULL, " \t\n"));
			break;
		}
	}

	fclose(stat);

	if (!lastloadstuff && !lastidle && !lastnice) {
		lastloadstuff = loadstuff;
		lastidle = idle;
		lastnice = nice;
		return 0;
	}

	loadline = (loadstuff - lastloadstuff) + (idle - lastidle);

	if (loadline)
		loadline = ((loadstuff - lastloadstuff) * 100) / loadline;
	else
		loadline = 100;

	lastloadstuff = loadstuff;
	lastidle = idle;
	lastnice = nice;

	return loadline;
}

void cpuReadFreq()
{
	FILE *fp;
	char red[10];

	cpuState.setFreqFile =
		"/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq";

	if ((fp = fopen(cpuState.setFreqFile, "r")) == NULL) {
		fprintf(stderr, "Error reading the info file (%s):\n%s\n",
			cpuState.setFreqFile, strerror(errno));
		free_and_exit(ERROR);
	}

	fgets(red, 10, fp);
	cpuState.actualFreq = atoi(red);

	fclose(fp);
}

void read_sysfs_temp(char *dir, char *file, double *value) {
	char filename[256];
	FILE *fp;

	snprintf(filename, 256, "/sys/class/thermal/%s/%s", dir, file);

	fp = fopen(filename, "r");
	if (fp) {
		fscanf(fp, "%lf", value);
                (*value) /= 1000.;
		fclose(fp);
        }
}

void read_applesmc_temp(char *file, double *value) {
	char filename[256];
	FILE *fp;

	snprintf(filename, 256, "/sys/devices/platform/applesmc.768/%s", file);

	fp = fopen(filename, "r");
	if (fp) {
		fscanf(fp, "%lf", value);
                (*value) /= 1000.;
		fclose(fp);
        }
}

void cpuReadTemp()
{
	DIR *dir;
	struct dirent *dirent;
	double cpuTemp = 0;

	if (apple) {
		read_applesmc_temp("temp8_input", &cpuTemp);
	} else {
		dir = opendir("/sys/class/thermal/");
		if (!dir) {
			printf("Could not open /sys/class/thermal\n");
			return;
		}
		while ((dirent = readdir(dir))) {
			if (strlen(dirent->d_name) < 3)
				continue;
			read_sysfs_temp(dirent->d_name, "temp", &cpuTemp);
		}
		closedir(dir);
	}
	cpuState.Temp = cpuTemp;
}
