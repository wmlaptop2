/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __MAIN_H__
#define __MAIN_H__

#include "version.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <time.h>
#include <errno.h>
#include <dirent.h>

#include <X11/Xlib.h>
#include <X11/xpm.h>
#include <X11/extensions/shape.h>

#include <sys/signal.h>
#include <sys/wait.h>

/* those includes are needed by alarm */
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/kd.h>

#define  XPM_NAME      wmlaptop_xpm

typedef unsigned char bool;

#ifndef true
# define true 1
#endif

#ifndef false
# define false 0
#endif

#define    ERROR        1
#define    SUCCESS      0


typedef unsigned char u_int8;
typedef unsigned short u_int16;
typedef unsigned int u_int32;


/****************************************
 * Every global variable of the program *
 * is declared in main.c, but here is   *
 * is declared as 'extern'              *
 ****************************************/
extern char **wmlaptop_xpm;

/* it keeps track of passing time:
 * updated with time(NULL) at every cycle */
extern        time_t secondsCounter;

/********************
 * BATTERIES' STUFF *
********************/

/*****************************************************************
 * For every battery present in the system will be allocated a   *
 * structure containing following informations                   *
 *                                                               *
 * present:     true if battery is present, false otherwise      *
 * error:       true if reading the info's file report an error: *
 *              this battery wont be used until present will     *
 *              become false (only ACPI)                         *
 * counter:     the counter for the battery (each battery has    *
 *              unique number ) (only ACPI)                      *
 * stateFile:   file path where to read information about actual *
 *              state of the battery (only ACPI)                 *
 * infoFile:    file path where to read general informations     *
 *              about the battery (only ACPI )                   *
 * capacity:    battery capacity (only ACPI)                     *
 * actualState: remaining battery capacity (only ACPI)           *
 * percentage:  remaining battery percentage (calculated with    *
 *              actualState divided by capacity  with ACPI       *
 *              reading /proc/apm con APM)                       *
 * useLFC:      use 'last full capacity' tag instead of 'design  *
 *              capacity' reading the information from info file *
 *              (only ACPI)                                      *
 * filler:      pointer to the function that will fill the       *
 *              fields (different if you're using APM or ACPI)   *
 *****************************************************************/
typedef struct battery_str * battery;

struct battery_str
{
	bool    present;
	bool    error;
	u_int8  counter;
	char    stateFile[52];
	char    infoFile[52];
	double  capacity;
	u_int32 actualState;
	u_int16 percentage;
	bool    useLFC;
	bool(*filler)(battery);
};

/*****************************************************************
 * this structure is used for keeping track of all batteries and *
 * it calculate estimated remaining time                         *
 *                                                               *
 * nBatt:         it shows the number of elements of             *
 *                BatteryVector; it is also the number of        *
 *                installed batteries, in case you're using ACPI *
 * batteryVector: vector containing all single battery           *
 *                informations (ACPI) or all batteries (APM)     *
 * percentage:    average of all batteries (nBatt) presents in   *
 *                in batteryVector. it's the value that will be  *
 *                showed in the dockapp                          *
 * remainingTime: remaining battery time, estimated calculating  *
 *                how much has power the laptop has used in the  *
 *                last ten minutes (this value could be modified)*
 * isCharging:    true if battery charger is plugged in.         *
 * updater:       pointer to function that is called             *
 *                (periodically) to update battery status.       *
 *                function that points is different if you're    *
 *                using ACPI or APM                              *
 *****************************************************************/
extern struct power_str powerState;

struct power_str
{
	u_int8        nBatt;
	battery     * batteryVector;
	u_int8        percentage;
	u_int16       remainingTime;
	bool          isCharging;
//	double        rate;
	void(*updater)();
};

extern double powerrate;
extern double capacity;

/*******************
 * CPUS' STUFF     *
 *****************************************************************
 * wmlaptop let the user to change cpu frequency on the fly, by  *
 * clicking on two arrows, but it can also handle by itself this *
 * responibility according with cpu load. it can decrease or     *
 * decrease cpu frequency when we're not doing an heavy use of it*
 * or increase it at its maximum when cpu load is at 100%.       *
 * in every case wmlaptop shows on its display the actual cpu    *
 * load, read and stored in the global variable cpuLoad at every *
 * events cycle.                                                 *
 *****************************************************************/
extern        u_int8  cpuLoad;

/*****************************************************************
 * global variable cpuState is a structure of this type, it      *
 * stores cpu status. here are the fields explained:             *
 * setFreqFile:pointer to the path to use for writing/reading    *
 *             cpu frequency                                     *
 * actualFreq: updated at every events cycle, it takes as value  *
 *             the actual cpu frequency                          *
 * Temp: the CPU temperature                                     *
 *****************************************************************/
struct cpuFreq
{
	u_int32 actualFreq;
	char *  setFreqFile;
	int     Temp;
};

extern struct cpuFreq cpuState;

/*******************
 * XLIB' STUFF     *
 *****************************************************************
 * code taken here and there for creating a dockapp with xlib    *
 *****************************************************************/

struct XpmIcon
{
	Pixmap			pixmap;
	Pixmap			mask;
	XpmAttributes	 attributes;
};

/* the X connection */
extern        Display   * display;
extern        Window      Root;
extern        Window      iconwin;
extern        Window      win;
extern        int         screen;
extern        int         x_fd;
extern        int         d_depth;
extern        XSizeHints  mysizehints;
extern        XWMHints    mywmhints;
extern        GC          NormalGC;
extern        XEvent      e;
extern struct XpmIcon     wmgen;
extern        Pixmap      pixmask;

/* the main image */
extern        char        wmlaptop_mask_bits[64 * 64];
extern        int         wmlaptop_mask_width;
extern        int         wmlaptop_mask_height;

  /*****************/
 /* Mouse Regions */
/*****************/

#define    MAX_MOUSE_REGION    3

#define    MREGION_AUTOFREQ    0
#define    MREGION_MINFREQ     1
#define    MREGION_MAXFREQ     2

struct mouseRegion
{
	u_int8     top;
	u_int8     bottom;
	u_int8     left;
	u_int8     right;
};

extern struct mouseRegion mouse_region[MAX_MOUSE_REGION];


#include "main.h"
#include "init.h"
#include "event.h"
#include "draw.h"
#include "battery.h"
#include "cpu.h"
#include "autoscript.h"


/*******************
 * alpha and omega *
 *******************/
int main(int, char **);

/* this one takes as argument the exit's code, but
 * before exiting, it frees any allocated memory */
void free_and_exit(int);

void usage(char *, char *);
void version();

/*
 * wmlaptop arguments
 */
void setArgs(int, char **);

/* this shows the default compiled settings and exit */
void defaultSettings(void);

#define    ARGSDEF_LEAVE              0

extern int cpu_update;
extern int battery_update;
extern int power_update;
extern int temp_update;
extern int session_idle;

float get_session_idle_time(Display *display);

extern char *XDisplay;

extern char auto_shutdown;
extern short shutdown_delay;
extern char auto_alarm;
extern bool dontBlink100;
extern bool quiet;

/* use keyboard backlight control on apple laptops */
extern int apple;

extern int manage_backlight;
int get_screen_brightness(void);

/* the QUIET macro */
#define    PRINTQ( output, frm, args... )                        \
                                                                 \
do {                                                             \
	if (quiet == false)                                  \
		fprintf( output, frm, ##args );                          \
} while ( 0 )

#endif
