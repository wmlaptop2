# modify this if you want/need

INSTALLDIR = /usr/local/bin/
CC = gcc

LIBDIR = -L/usr/X11R6/lib
LIBS   = `pkg-config --libs dbus-1 --libs dbus-glib-1` -lXpm -lXext -lX11 -lXss
CFLAGS = `pkg-config --cflags dbus-1 --cflags dbus-glib-1` -Wall -g
OBJS   =  main.o init.o event.o draw.o battery.o cpu.o autoscript.o pixmap.o apple.o
EXE    = wmlaptop2


export INSTALLDIR
export LIBDIR
export LIBS
export CFLAGS
export OBJS
export EXE
export CC

all:
	cd src && make

clean:
	cd src && make clean

install:
	cd src && make install
	./.info

uninstall:
	cd src && make uninstall

reall: clean all
